#include <stdio.h>
int main(int argc, char const *argv[])
{
  int arreglo_numerico[10]={10,20,30,40,50,60,70,80,90,100};
  // EXTRAEMOS LOS VALORES INDICANDO SU SUBINDICE
  printf("El subindice 0 es: %d\n",arreglo_numerico[0]);
  printf("El subindice 1 es: %d\n",arreglo_numerico[1]);
  printf("El subindice 2 es: %d\n",arreglo_numerico[2]);
  printf("El subindice 3 es: %d\n",arreglo_numerico[3]);
  printf("El subindice 4 es: %d\n",arreglo_numerico[4]);
  printf("El subindice 5 es: %d\n",arreglo_numerico[5]);
  printf("El subindice 6 es: %d\n",arreglo_numerico[6]);
  printf("El subindice 7 es: %d\n",arreglo_numerico[7]);
  printf("El subindice 8 es: %d\n",arreglo_numerico[8]);
  printf("El subindice 9 es: %d\n",arreglo_numerico[9]);
  return 0;
}
