struct autoStruct{
int anio;
char nombreDelAuto[30];
char tipo[15];
};
typedef struct autoStruct AUTO;

struct computadora{
  char modelo[15];
  char procesador[20];
  char grafica[20];
};
typedef struct computadora COMPUTADORA;

struct alumno{
int edad;
char nombre[60];
char genero;
char carrera[25];
char nCuenta[10];
};
typedef struct alumno ALUMNO;

struct equipoFutbol{
  char nombre[30];
  int integrantes;
  int anios activos;
  char liga[25];
};
typedef struct equipoFutbol FUTBOL;

struct mascota{
char nombre[25];
int edad;
char raza[15];

};
typedef struct mascota MASCOTA;

struct videJuego{
  char nombre[25];
  char desarrollador[25];
  int anioDePresentacion;
};
typedef struct videJuego JUEGO;

struct superHeroe{
  char nombreDeSuper[25];
  char nombreNormal[25];
  int anioDeExistencia;
  char poderEspecial[25];

};
typedef struct superHeroe Heroe;

struct planeta{
  char nombreDelPlaneta[25];
  char nombreDelSistema[25];
  char nombreDeLaGalaxia[25];
  int aniosLusDeLaTierra;
  char tipo[15];


};
typedef struct planeta PLANETA;

struct personajeJuegoFavorito{
  char nombreDelPersonaje[25];
  char juegoEnElQueSale[25];
};
typedef struct personajeJuegoFavorito PERSONAJUEGO;

struct comic{
  char nombre[40];
  int fechaDeSalida;
  float version;
};
typedef struct comic COMIC;
