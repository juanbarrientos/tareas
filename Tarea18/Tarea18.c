#include <stdio.h>
#define Becalos 15
int main(int argc, char const *argv[])
{
  int opcion=0;
  float dinero_disponible=0;
  float dinero_retirar=0;
  float dinero_depositado=0;
  char bec=0;
  char con=0;
    printf("¿Con cuanto dinero quiere iniciar el programa?: ");
    scanf("%f", &dinero_disponible);
    do {
    printf("Elija una opción.\n1)Depositar.\n2)Retirar.\n3)Donar a Becalos ($15).\n: ");
    scanf("%d", &opcion);
    switch (opcion)
    {
      case 1:
      printf("¿Cuánto desea depositar?: ");
      scanf("%f",&dinero_depositado);
      dinero_disponible=dinero_depositado+dinero_disponible;
      printf("Deposite el dinero........................Depositado\nUsted ha depositado:%.3f\nSu Saldo actual es: %.3f pesos\n",dinero_depositado,dinero_disponible);
      break;
      case 2:
      printf("¿Cuánto desea retirar?: ");
      scanf("%f",&dinero_retirar);
      if (dinero_disponible>=dinero_retirar)
      {
        dinero_disponible=dinero_disponible-dinero_retirar;
        printf("Recoja su dinero\nSu dinero restante es %.3f pesos\n",dinero_disponible);
      }
      else
      {
        printf("No tiene el Saldo suficiente....\nSu saldo actual es %.3f pesos\n",dinero_disponible);
      }
      break;
      case 3:
      printf("¿Esta seguro que desea donar(S/N)?: ");
      scanf(" %c", &bec);
      if (bec=='S'|bec=='s')
      {
        if (dinero_disponible>=Becalos)
        {
          dinero_disponible=dinero_disponible-Becalos;
          printf("Donando.....\nListo su Saldo es: %.3f pesos\n",dinero_disponible);
        }
        else
        {
          printf("No tiene el Saldo suficiente....\nSu saldo actual es %.3f pesos\n",dinero_disponible);
        }
      }
      break;
      default:
      printf("Opción Incorrecta\n");
      }
    printf("Quiere Continuar(S/N): ");
    scanf(" %c", &con);
  } while(con=='S'|con=='s');
  return 0;
}
